from flask import Flask, render_template, request
import time
import array as arr
import math

app = Flask(__name__)

validaciones = {
    "INPUT_VACIO" : "Por favor, ingrese un número.",
    "INPUT_NUMERICO" : "Por favor, ingrese un valor numérico.",
    "INPUT_RANGO_MINIMO" : "Por favor, ingrese un número mayor o igual a 1"
}
       
@app.route('/', methods=["GET"])
def home():
    return render_template("index.html")


@app.route('/', methods=["POST"])
def mostrarNombre():
    inicio = time.time()  
    numero = request.form["numero"]

    # Se eliminan los espacios en blanco
    numero = numero.strip()

    # Se valida el valor ingresado
    if (numero == ""):     
        return render_template("index.html", msj = validaciones["INPUT_VACIO"], reiniciarVerificador = False)
    elif(numero.isdigit() is False):    
         return render_template("index.html", msj = validaciones["INPUT_NUMERICO"], reiniciarVerificador = False)         
    elif (int(numero) < 1):
         return render_template("index.html", msj = validaciones["INPUT_RANGO_MINIMO"], reiniciarVerificador = False)
    else:  
        esPrimo = False
        divisores = []
     
        # Se obtienen los divisores del número ingresado
        i = 1
        while i <= (int(numero)**0.5):
            if (int(numero) % i == 0):
                divisores.append(i)
                if (int(numero) // i != i):
                     divisores.append(int(numero) // i)
            i += 1
        
        # Si la cantidad de divisores son exactamente dos (1 y el propio número), el número ingresao es primo 
        if ((len(divisores)) == 2):
            esPrimo= True
        
        fin = time.time() 
     
        # Se obtienen el tiempo insumido por el algoritmo, calculado en minutos y segundos
        tiempoDeEjecucion = fin - inicio
        minutos, segundos = divmod(tiempoDeEjecucion, 60)

        # Se formatean los minutos y segundos para que incluyan dos decimales
        minutosFormat = "{:02d}".format(int(minutos))
        segundosFormat = "{:02d}".format(int(segundos))

        return render_template("index.html", numero = numero, esPrimo = esPrimo, minutos = minutosFormat, segundos = segundosFormat, divisores = sorted(divisores), reiniciarVerificador = False)
    

if __name__ == "__main__":
    app.run(debug=True, port=5000)